
#/bin/bash
#
#
USB=$(lsusb | grep Google)
echo $USB
if [[ -z $USB ]]; then
  echo "Device not found on USB. Are you seeing it as being connected?"
  exit 2;
fi
apt update
apt install wget unzip -y
wget https://dl.google.com/android/repository/platform-tools-latest-linux.zip
unzip $(ls | grep platform)
wget https://dl.google.com/dl/android/aosp/barbet-rd2a.211001.002-factory-dbed3a6f.zip
unzip barbet-rd2a.211001.002-factory-dbed3a6f.zip
cd barbet-rd2a.211001.002/
../platform-tools/fastboot --version
sleep 10
../platform-tools/fastboot flash bootloader bootloader-barbet-b9-0.4-7265224.img
../platform-tools/fastboot reboot-bootloader
sleep 5
../platform-tools/fastboot flash radio radio-barbet-g7250-00132-210517-b-7370009.img
../platform-tools/fastboot reboot-bootloader
sleep 5
../platform-tools/fastboot update image-barbet-rd2a.211001.002.zip --skip-reboot
sleep 15
echo "Device Provisioned. Add the..."
echo "carbonite.nowsecure.io/provision-needed: \"\" " 
echo "...annotation to the SoloDevice entry in Cluser Explorer or just provision the device through the Carbonite UI is it's listed there."
#../platform-tools/fastboot devices
